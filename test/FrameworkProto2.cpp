// frameworkproto1.cpp : Defines the entry point for the console application.
//

#include <thread>
#include <future>
#include <vector>
#include <memory>
#include <iostream>
#include <string>
#include <algorithm>
#include <mutex>

#include <random>

class Semaphore
{
public:
    Semaphore() : mCount(0)
    {

    }

    inline void post()
    {
        std::unique_lock<std::mutex> lock(mMutex);
        mCount++;
        mCondition.notify_one();
    }

    inline void wait()
    {
        std::unique_lock<std::mutex> lock(mMutex);

        // Wait until the count is posted above zero.
        mCondition.wait(lock, [&] { return mCount > 0; });

        mCount--;
    }


private:
    std::mutex mMutex;
    std::condition_variable mCondition;
    int mCount;
};


class ThreadFence
{
public:
    ThreadFence(int numThreads) 
        : mCount(0), 
          mThreads(numThreads), 
          mWaiting(0)
    {

    }
    
    void wait()
    {
        std::unique_lock<std::mutex> lock(mMutex);
        mCount++; 
        mWaiting++;

        
        // Once all of the threads are waiting, each one can wake up.
        mCondition.wait(lock, [&] { return mCount >= mThreads; });
     
        if (--mWaiting == 0) {
            mCount = 0;
        }

        // Unlock our guard and notify all the threads waiting on the condition 
        // to check the predicate.  They should all pass and eventually the number
        // waiting will reach zero meaning the fence can then be reused.
        lock.unlock();
        mCondition.notify_all();
    }


private:
    std::mutex mMutex, mMutex2;
    std::condition_variable mCondition;
    int mCount, mThreads;
    std::atomic_int mWaiting;
};

std::atomic_bool g_Finished = false;

//std::mutex g_Mutex;
//std::condition_variable g_Cond;
//
//Semaphore mSem;

ThreadFence g_Fence(3), g_FenceStart(3);

int g_NumProc1Runs, g_NumProc2Runs, g_NumCameraFrames, g_DroppedFrames;

void someWork(int delay) {
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
}


void proc1()
{
    while (!g_Finished) {

        // Await camera start
        g_FenceStart.wait();

        someWork(10);
        g_NumProc1Runs++;
        g_Fence.wait();
        //fprintf(stderr, "A");
        //mSem.post();
        /*std::this_thread::sleep_for(std::chrono::milliseconds(40));
        fprintf(stderr,"a");*/
    }
}

void proc2()
{
    std::default_random_engine rt;
    std::uniform_int_distribution<int> dist(30, 90);

    while (!g_Finished) {
        // Await cmaera frame
        g_FenceStart.wait();

        // Simulate a process that occasionally takes too much time.
        someWork( (g_NumProc2Runs % 200) == 0 ? dist(rt) : 15 );
        g_NumProc2Runs++;

        // Signal we're done.
        g_Fence.wait();
        

    }
}

const int MaxFrameTime = 33;

void cameraProc()
{
    auto start = std::chrono::high_resolution_clock::now();
    auto frameStart = std::chrono::high_resolution_clock::now();

    // Number of camera frames seen since our last averaging step.
    int numCameraFramesSlice = 0;

    // The UI thread will set this to true when we should exit.
    while (!g_Finished) {
        
        // New frame starts here.
        g_FenceStart.wait();
        frameStart = std::chrono::high_resolution_clock::now();
        
        g_NumCameraFrames++;
        numCameraFramesSlice++;

        // Wait for processor threads to hit their end frame mark.
        g_Fence.wait();

        auto frameEnd = std::chrono::high_resolution_clock::now();
        if (g_NumCameraFrames % 20 == 0) {
            auto end = std::chrono::high_resolution_clock::now();
            
            auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            int avg = (int)(durationMS / numCameraFramesSlice);
            printf("Total time: %d ms. Average: %d ms\n", (int)durationMS, avg);

            //printf(".");
            fflush(stdout);
    
            // Reset the start time.
            start = std::chrono::high_resolution_clock::now();
            numCameraFramesSlice = 0;
        }

        // Check for being over our frame time budget.
        auto frameDurationMS = std::chrono::duration_cast<std::chrono::milliseconds>(frameEnd - frameStart).count();
        if (frameDurationMS > MaxFrameTime) {

            printf("Over budget: %d ms!\n", frameDurationMS - MaxFrameTime);

            // Drop frame(s).
            int timeToSkip = frameDurationMS - MaxFrameTime;
            while (timeToSkip > 0) {
                printf("Skipping frame!\n");
                timeToSkip -= MaxFrameTime;

                // Increase total frame counter.
                g_NumCameraFrames++;
                g_DroppedFrames++;
            }
        }
        else {
            // Had some time left over.
            int timeTillNextFrame = MaxFrameTime - frameDurationMS;

            // Signal a new camera frame every 33ms (60fps)
            std::this_thread::sleep_for(std::chrono::milliseconds(timeTillNextFrame));
        }
    }
}

int main()
{
    auto start = std::chrono::high_resolution_clock::now();
	auto clockThread = std::thread(cameraProc);

	// Processor1
	auto thread1 = std::thread(proc1);

	// Processor2
	auto thread2 = std::thread(proc2);

	// Block waiting for user to enter 'q'
	std::string input;
	std::cout << "Enter 'q' to quit." << std::endl;
	while(!g_Finished) {
		std::getline(std::cin, input);
		if(input == "q") {
			g_Finished = true;
			std::cout << "Exiting..." << std::endl;
		}
	}
    
    auto end = std::chrono::high_resolution_clock::now();
    printf("Ran %d camera frames, processing 1: %d times, processing 2: %d times.\nDropped Frames: %d", 
           g_NumCameraFrames, g_NumProc1Runs, g_NumProc2Runs, g_DroppedFrames);

    auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    printf("Total time: %d ms. Average: %d ms\n", 
           (int)durationMS, (int)(durationMS / g_NumCameraFrames));

    for(int i = 0; i < 3; i++ )
        g_FenceStart.wait();
        g_Fence.wait();

	clockThread.join();
	thread1.join();
	thread2.join();

    return 0;
}

